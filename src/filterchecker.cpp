/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "filterchecker.h"


FilterChecker::FilterChecker(QObject *parent) : QObject(parent)
{
    qDebug() << "FilterChecker created";
}

FilterChecker::~FilterChecker()
{
    qDebug() << "FilterChecker destroyed";
}


/*
 * Set or update filter rules; called from the FilterEditor
 *
 */
void FilterChecker::setFilters(QVariantList newFiltersList)
{
    m_filteredContent.clear();
    m_filteredAuthor.clear();
    m_filteredGenerator.clear();
    m_filteredDescription.clear();

    // Define new filter values
    foreach (const QVariant filter, newFiltersList)
    {
        const int filterType = filter.toMap().value(QStringLiteral("type"))
                                             .toInt();

        QStringList pair = QStringList{ filter.toMap()
                                              .value(QStringLiteral("text"))
                                              .toString(),
                                        filter.toMap()
                                              .value(QStringLiteral("action"))
                                              .toString() };

        switch (filterType)
        {
        case 0: // post content
            m_filteredContent.append(pair);
            break;

        case 1: // author
            m_filteredAuthor.append(pair);
            break;

        case 2: // application (generator)
            m_filteredGenerator.append(pair);
            break;

        case 3: // activity description
            m_filteredDescription.append(pair);
            break;

        default:
            break;
        }
    }

    qDebug() << "FilterChecker() filters updated, with"
             << newFiltersList.length() << "filters";
}



/*
 * Return NoFiltering, FilterOut or Highlight
 *
 */
int FilterChecker::validateActivity(ASActivity *activity)
{
    QVariantMap filterMatches;

    int filterAction = NoFiltering;  // Innocent until proven guilty!
    QStringList matchingContent;
    QStringList matchingAuthor;
    QStringList matchingGenerator;
    QStringList matchingDescription;


    const QString postTitle = activity->object()->getTitle();
    const QString postContents = activity->object()->getContent();
    foreach (const QStringList contents, m_filteredContent)
    {
        if (postContents.contains(contents.first(), Qt::CaseInsensitive)
         || postTitle.contains(contents.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Post Content:"
                     << contents.first();

            filterAction = contents.last().toInt();
            matchingContent.append(contents.first());
        }
    }

    const QString activityAuthorId = activity->author()->getId();
    const QString objectAuthorId = activity->object()->author()->getId();
    foreach (const QStringList authorId, m_filteredAuthor)
    {
        if (activityAuthorId.contains(authorId.first(), Qt::CaseInsensitive)
         || objectAuthorId.contains(authorId.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Author ID:"
                     << authorId.first();

            filterAction = authorId.last().toInt();
            matchingAuthor.append(authorId.first());
        }
    }


    const QString activityGenerator = activity->getGenerator();
    foreach (const QStringList generator, m_filteredGenerator)
    {
        if (activityGenerator.contains(generator.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Application (generator):"
                     << generator.first();

            filterAction = generator.last().toInt();
            matchingGenerator.append(generator.first());
        }
    }

    // Remove links from activity description, so it's easier to write filtering rules
    const QString activityDescription = MiscHelpers::htmlWithoutLinks(activity->getContent());
    foreach (const QStringList description, m_filteredDescription)
    {
        if (activityDescription.contains(description.first(), Qt::CaseInsensitive))
        {
            qDebug() << "Filtering item because of Activity Description:"
                     << description.first();

            filterAction = description.last().toInt();
            matchingDescription.append(description.first());
        }
    }


    filterMatches.insert(QStringLiteral("filterAction"),
                         filterAction);
    filterMatches.insert(QStringLiteral("matchingContent"),
                         matchingContent);
    filterMatches.insert(QStringLiteral("matchingAuthor"),
                         matchingAuthor);
    filterMatches.insert(QStringLiteral("matchingGenerator"),
                         matchingGenerator);
    filterMatches.insert(QStringLiteral("matchingDescription"),
                         matchingDescription);

    activity->setFilterMatches(filterMatches);


    return filterAction;
}
