/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef MINORFEED_H
#define MINORFEED_H

#include <QFrame>
#include <QVBoxLayout>
#include <QFont>
#include <QVariantList>
#include <QPushButton>

#include <QDebug>

#include "pumpcontroller.h"
#include "globalobject.h"
#include "mischelpers.h"
#include "minorfeeditem.h"
#include "asactivity.h"
#include "filterchecker.h"


class MinorFeed : public QFrame
{
    Q_OBJECT

public:
    explicit MinorFeed(PumpController::requestTypes minorFeedType,
                       PumpController *pumpController,
                       GlobalObject *globalObject,
                       FilterChecker *filterChecker,
                       QWidget *parent = 0);
    ~MinorFeed();

    void clearContents();
    void removeOldItems(int minimumToKeep);
    void insertSeparator(int position);
    void markAllAsRead();
    void updateFuzzyTimestamps();
    void syncActivityWithTimelines(ASActivity *activity);


signals:
    void newItemsCountChanged(int itemCount, int highlightedCount);
    void newItemsReceived(PumpController::requestTypes m_feedType,
                          int itemCount, int highlightedCount,
                          int filteredCount, int pendingForNextUpdate);

    void objectUpdated(ASObject *object);
    void objectLiked(QString objectId, QString objectType, QString actorId,
                     QString actorName, QString actorUrl);
    void objectUnliked(QString objectId, QString objectType, QString actorId);
    void objectReplyAdded(ASObject *object);
    void objectDeleted(ASObject *object);


public slots:
    void updateFeed();
    void getMoreActivities();
    void setFeedContents(QVariantList activitiesList,
                         QString previous, QString next,
                         int totalItemCount);
    void onUpdateFailed(int requestType);

    void decreaseNewItemsCount(bool wasHighlighted);

    void updateAvatarFollowStates();


private:
    QString m_prevLink;
    QString m_nextLink;
    int m_fullFeedItemCount;
    int m_pendingToReceiveNextTime;

    PumpController::requestTypes m_feedType;
    int m_feedBatchItemCount;
    bool m_firstLoad;
    bool m_gettingNew;

    QString m_previousNewestActivityId;
    QList<MinorFeedItem *> m_itemsInFeed;

    int m_unreadItemsCount;
    int m_highlightedItemsCount;

    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_itemsLayout;
    QPushButton *m_getPendingButton;
    QPushButton *m_getOlderButton;

    QFrame *m_separatorFrame;


    PumpController *m_pumpController;
    GlobalObject *m_globalObject;
    FilterChecker *m_filterChecker;
};

#endif // MINORFEED_H
