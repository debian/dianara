/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef PROXYDIALOG_H
#define PROXYDIALOG_H

#include <QWidget>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QLabel>
#include <QAction>
#include <QSettings>

#include <QDebug>


class ProxyDialog : public QWidget
{
    Q_OBJECT

public:
    explicit ProxyDialog(int proxyType,
                         QString hostname, QString port,
                         bool useAuth,
                         QString user, QString password,
                         QWidget *parent = 0);
    ~ProxyDialog();

signals:

public slots:
    void toggleAuth(bool state);

    void saveSettings();

private:
    QVBoxLayout *m_mainLayout;
    QFormLayout *m_fieldsLayout;
    QHBoxLayout *m_buttonsLayout;

    QComboBox *m_proxyTypeComboBox;
    QLineEdit *m_hostnameLineEdit;
    QLineEdit *m_portLineEdit;
    QCheckBox *m_authCheckBox;
    QLineEdit *m_userLineEdit;
    QLineEdit *m_passwordLineEdit;
    QLabel *m_passwordNoteLabel;

    QPushButton *m_saveButton;
    QPushButton *m_cancelButton;

    QAction *m_closeAction;
};

#endif // PROXYDIALOG_H
