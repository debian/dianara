/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#ifndef GROUPSMANAGER_H
#define GROUPSMANAGER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QAction>
#include <QMessageBox>

#include <QDebug>

#include "pumpcontroller.h"


class GroupsManager : public QWidget
{
    Q_OBJECT

public:
    explicit GroupsManager(PumpController *pumpController,
                           QWidget *parent = 0);
    ~GroupsManager();


signals:


public slots:
    void createGroup();
    void deleteGroup();
    void joinGroup();
    void leaveGroup();


private:
    QVBoxLayout *m_layout;

    QLineEdit *m_newGroupNameLineEdit;
    QLineEdit *m_newGroupSummaryLineEdit;
    QLineEdit *m_newGroupDescLineEdit;
    QPushButton *m_createGroupButton;

    QLineEdit *m_joinLeaveGroupIdLineEdit;
    QPushButton *m_joinGroupButton;
    QPushButton *m_leaveGroupButton;

    QAction *m_closeAction;

    PumpController *m_pumpController;
};

#endif // GROUPSMANAGER_H
