/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "dbusinterface.h"

DBusInterface::DBusInterface(QObject *parent) : QObject(parent)
{
    qDebug() << "DBusInterface created";
}

DBusInterface::~DBusInterface()
{
    qDebug() << "DBusInterface destroyed";
}


/****************************************************************************/
/******************************** SLOTS *************************************/
/****************************************************************************/


void DBusInterface::toggle()
{
    qDebug() << "DBusInterface::toggle()";
    QMetaObject::invokeMethod(parent(), "toggleMainWindow");
}


void DBusInterface::post(QString title, QString content)
{
    qDebug() << "DBusInterface::post(); "
             << "Title:" << title << "; "
             << "Text:" << content;

    QMetaObject::invokeMethod(parent(), "startPost",
                              Q_ARG(QString, title), Q_ARG(QString, content));
}

