/*
 *   This file is part of Dianara
 *   Copyright 2012-2022  JanKusanagi JRR <jancoding@gmx.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .
 */

#include "colorpicker.h"

ColorPicker::ColorPicker(QString description,
                         QString initialColorString,
                         QWidget *parent) : QWidget(parent)
{
    m_descriptionLabel = new QLabel(description, this);
    m_descriptionLabel->setWordWrap(true);
    m_descriptionLabel->setAlignment(Qt::AlignTop);

    m_checkBox = new QCheckBox(this);

    m_buttonPixmap = QPixmap(32, 32);

    m_button = new QPushButton(tr("Change..."), this);
    m_button->setIconSize(QSize(32, 32));
    m_button->setDisabled(true); // Disabled initially
    connect(m_button, &QAbstractButton::clicked,
            this, &ColorPicker::changeColor);

    connect(m_checkBox, &QAbstractButton::toggled,
            m_button, &QWidget::setEnabled);

    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_descriptionLabel, 10);
    m_layout->addSpacing(4);
    m_layout->addStretch(1);
    m_layout->addWidget(m_checkBox,         0);
    m_layout->addSpacing(8);
    m_layout->addWidget(m_button,           0);
    this->setLayout(m_layout);


    QColor initialColor(initialColorString);
    if (initialColor.isValid())
    {
        m_currentColor = initialColor;
        m_checkBox->setChecked(true);
    }
    else
    {
        if (initialColorString.startsWith(QStringLiteral("DISABLED")))
        {
            m_currentColor = initialColorString.remove(QStringLiteral("DISABLED"));
        }
        else
        {
            m_currentColor = QColor(Qt::blue);
        }
    }
    this->setButtonColor(m_currentColor);


    qDebug() << "ColorPicker created";
}


ColorPicker::~ColorPicker()
{
    qDebug() << "ColorPicker destroyed";
}


void ColorPicker::setButtonColor(QColor color)
{
    m_buttonPixmap.fill(color);
    m_button->setIcon(QIcon(m_buttonPixmap));
}


QString ColorPicker::getCurrentColor()
{
    if (m_checkBox->isChecked())
    {
        return m_currentColor.name(); // return in #RRGGBB format
    }
    else
    {
        // Return invalid color, but keeping the numeric code
        return QString("DISABLED%1").arg(m_currentColor.name());
    }
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SLOTS ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////


void ColorPicker::changeColor()
{
    QColor newColor = QColorDialog::getColor(m_currentColor,
                                             this,
                                             tr("Choose a color"));
    if (newColor.isValid())
    {
        m_currentColor = newColor;

        setButtonColor(m_currentColor);
    }

    qDebug() << "New color:" << m_currentColor;
}
